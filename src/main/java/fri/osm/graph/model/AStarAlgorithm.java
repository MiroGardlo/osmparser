package fri.osm.graph.model;

import java.util.PriorityQueue;

public class AStarAlgorithm extends Dijkstra {

    public AStarAlgorithm(Graph graph) {
        super(graph);
    }

    private double euclidianDistance(GraphNode node1, GraphNode node2) {
        return node1.computeDistance(node2);
    }

    @Override
    public double run(int nodeToStartId, int intNodeToFinishId) {
        GraphNode endNode = graph.getNode(intNodeToFinishId);
        System.out.println(euclidianDistance(graph.getNode(nodeToStartId), endNode));
        nodes = new PriorityQueue<>((o1, o2) ->
                (int) Math.signum((o1.getDistanceEstimate() + euclidianDistance(o1, endNode))
                        - (o2.getDistanceEstimate() + euclidianDistance(o2, endNode))));
        return super.run(nodeToStartId, intNodeToFinishId);
    }
}
