package fri.osm.graph.model;

import java.util.ArrayList;
import java.util.List;

public class GraphNode {
    private long id;
    private List<Neighbour> neighbours;
    private double latitude;
    private double longitude;
    private double distanceEstimate;


    public GraphNode(fri.osm.model.Node node) {
        this.id = node.getId();
        this.neighbours = new ArrayList<>();
        this.latitude = node.getLatitude();
        this.longitude = node.getLongitude();
        this.distanceEstimate = Double.MAX_VALUE;
    }

    public GraphNode(String line) {
        String[] input = line.split("\\s");
        this.neighbours = new ArrayList<>();
        this.id = Integer.valueOf(input[0]);
        //this.latitude = Double.valueOf(input[1]);
        //this.longitude = Double.valueOf(input[2]);
        this.distanceEstimate = Integer.MAX_VALUE;
    }

    public double computeDistance(GraphNode node) {
        int R = 6371;
        double dLat = Math.toRadians(node.getLatitude() - latitude);
        double dLon = Math.toRadians(node.getLongitude() - longitude);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(node.getLatitude())) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c;
    }


    public List<Neighbour> getNeighbours() {
        return neighbours;
    }

    public void addNeighbour(GraphNode node, EdgeInfo edgeInfo) {
        neighbours.add(new Neighbour(node, edgeInfo));
    }

    public void setDistanceEstimate(double distanceEstimate) {
        this.distanceEstimate = distanceEstimate;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getDistanceEstimate() {
        return distanceEstimate;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
