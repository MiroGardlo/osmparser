package fri.osm.graph.model;


import java.util.ArrayList;
import java.util.List;

public class NodeCounter {
    private GraphNode node;
    private int numberOfReferences;
    private List<Long> referencedInWays;

    public NodeCounter(GraphNode node) {
        this.node = node;
        this.referencedInWays = new ArrayList<>(10);
        this.numberOfReferences = 0;
    }

    public GraphNode getNode() {
        return node;
    }

    public void setNode(GraphNode node) {
        this.node = node;
    }

    public int getNumberOfReferences() {
        return numberOfReferences;
    }

    public void addReference(long wayId) {
        this.numberOfReferences++;
        this.referencedInWays.add(wayId);
    }
}
