package fri.osm.graph.model;

import java.util.List;
import java.util.PriorityQueue;


public class Dijkstra {
    protected PriorityQueue<GraphNode> nodes;
    protected Graph graph;
    protected int numberOfProcessedNodes;

    public Dijkstra(Graph graph) {
        this.nodes = new PriorityQueue<>((o1, o2) ->
                (int) Math.signum(o1.getDistanceEstimate() - o2.getDistanceEstimate()));
        this.graph = graph;
        this.numberOfProcessedNodes = 0;
    }

    public double run(int nodeToStartId, int intNodeToFinishId) {
        GraphNode startingNode = graph.getNode(nodeToStartId);
        GraphNode endNode = graph.getNode(intNodeToFinishId);
        startingNode.setDistanceEstimate(0);
        numberOfProcessedNodes = 0;
        nodes.add(startingNode);
        boolean iterate = true;
        do {
            GraphNode currentNode = nodes.poll();
            numberOfProcessedNodes++;
            if (currentNode == endNode) {
                iterate = false;
            } else {
                processNode(currentNode);

            }
        } while (iterate);
        return endNode.getDistanceEstimate();
    }

    public int getNumberOfProcessedNodes() {
        return numberOfProcessedNodes;
    }

    private void processNode(GraphNode currentNode) {
        List<Neighbour> neighbours = currentNode.getNeighbours();
        neighbours
                .stream()
                .forEach(neighbour ->
                        setPathLength(currentNode, neighbour)
                );

    }

    private void setPathLength(GraphNode nodeFrom, Neighbour neighbour) {
        if (nodeFrom.getDistanceEstimate() + neighbour.getInfo().getLength() < neighbour.getNode().getDistanceEstimate()) {
            neighbour.getNode().setDistanceEstimate(nodeFrom.getDistanceEstimate() + neighbour.getInfo().getLength());
            nodes.add(neighbour.getNode());
        }
    }
}
