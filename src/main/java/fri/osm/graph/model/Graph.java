package fri.osm.graph.model;

import java.util.*;

public class Graph {
    private HashMap<Long, GraphNode> nodes;

    public Graph(HashMap<Long, GraphNode> nodes) {
        this.nodes = nodes;
    }

    public void resetDistanceEstimate() {
        nodes.values().stream().forEach(node -> node.setDistanceEstimate(Integer.MAX_VALUE));
    }

    public boolean hasNode(long id) {
        return nodes.containsKey(id);
    }

    public void addNode(GraphNode node) {
        this.nodes.put(node.getId(), node);
    }

    public GraphNode getNode(long id) {
        return nodes.get(id);
    }

    public List<GraphNode> getNodes() {
        return new ArrayList<>(nodes.values());
    }

    public boolean isGraphConnected() {
        Stack<GraphNode> stack = new Stack<>();
        HashSet<GraphNode> visited = new HashSet<>(stack.size());
        stack.push(this.nodes.values().stream().findFirst().get());
        while (!stack.isEmpty()) {
            GraphNode current = stack.pop();
            if (!visited.contains(current)) {
                visited.add(current);
                current.getNeighbours()
                        .stream()
                        .filter(neighbour -> visited.contains(neighbour.getNode()))
                        .forEach(neighbour -> stack.push(neighbour.getNode()));
            }

        }
        return visited.size() == this.nodes.size();
    }
}
