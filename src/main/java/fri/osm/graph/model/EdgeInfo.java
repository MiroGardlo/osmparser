package fri.osm.graph.model;

/**
 * Created by Miroslav on 8.3.2017.
 */
public class EdgeInfo {
    private long id;
    private double length;

    public EdgeInfo(long id, double length) {
        this.id = id;
        this.length = length;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
}
