package fri.osm.graph.model;

public class Neighbour {
    private GraphNode node;
    private EdgeInfo info;

    public Neighbour(GraphNode node, EdgeInfo info) {
        this.node = node;
        this.info = info;
    }

    public GraphNode getNode() {
        return node;
    }

    public void setNode(GraphNode node) {
        this.node = node;
    }

    public EdgeInfo getInfo() {
        return info;
    }

    public void setInfo(EdgeInfo info) {
        this.info = info;
    }
}
