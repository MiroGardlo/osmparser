package fri.osm.graph;

import fri.osm.graph.model.EdgeInfo;
import fri.osm.graph.model.Graph;
import fri.osm.graph.model.GraphNode;
import fri.osm.graph.model.NodeCounter;
import fri.osm.model.HelperWay;
import fri.osm.model.NodalReference;
import fri.osm.model.OSMStructure;
import fri.osm.model.Way;
import fri.osm.service.DirectionHelper;

import java.util.ArrayList;
import java.util.HashMap;


public class OSMFilter {
    private OSMStructure osmStructure;
    private HashMap<Long, NodeCounter> nodes;
    private HashMap<Long, HelperWay> ways;


    public OSMFilter(OSMStructure osmStructure) {
        this.nodes = new HashMap<>(osmStructure.numberOfNodes());
        this.ways = new HashMap<>(osmStructure.numberOfWays());
        this.osmStructure = osmStructure;
    }

    public Graph init() {
        initNodes();
        initCounts();
        return new Graph(initWays());
    }

    private void initNodes() {
        osmStructure
                .getNodes()
                .stream()
                .forEach(node ->
                        nodes.put(node.getId(), new NodeCounter(new GraphNode(node)))
                );
    }

    private void initCounts() {
        osmStructure
                .getWays()
                .stream()
                .forEach(way ->
                        way
                                .getReferencedNodes()
                                .stream()
                                .forEach(nodalReference ->
                                        nodes.get(nodalReference.getReferenceId()).addReference(way.getId()))
                );
    }

    private HashMap<Long, GraphNode> initWays() {
        HashMap<Long, GraphNode> nodes = new HashMap<>(this.nodes.size());
        osmStructure
                .getWays()
                .stream()
                .forEach(way -> {
                    HelperWay helperWay = new HelperWay(way);
                    ways.put(way.getId(), helperWay);
                    boolean wayOneWay = DirectionHelper.isWayOneWay(helperWay);
                    processWay(way, nodes, wayOneWay);
                });
        return nodes;
    }

    private void processWay(Way way, HashMap<Long, GraphNode> nodes, boolean isOneWay) {
        ArrayList<NodalReference> referencedNodes = way.getReferencedNodes();
        NodeCounter firstNode = this.nodes.get(referencedNodes.get(0).getReferenceId());
        NodeCounter lastNode = this.nodes.get(referencedNodes.get(referencedNodes.size() - 1).getReferenceId());
        int currentNodeNumber = 1;
        NodeCounter parentNode = firstNode;
        NodeCounter previousNode = firstNode;
        double currentDistance = 0;
        nodes.put(firstNode.getNode().getId(), firstNode.getNode());
        nodes.put(lastNode.getNode().getId(), lastNode.getNode());
        while (currentNodeNumber < referencedNodes.size() - 1) {
            NodeCounter current = this.nodes.get(referencedNodes.get(currentNodeNumber).getReferenceId());
            currentDistance += previousNode.getNode().computeDistance(current.getNode());
            if (current.getNumberOfReferences() > 1) {
                nodes.put(current.getNode().getId(), current.getNode());
                EdgeInfo edgeInfo = new EdgeInfo(way.getId(), currentDistance);
                parentNode.getNode().addNeighbour(current.getNode(), edgeInfo);
                if (isOneWay) {
                    current.getNode().addNeighbour(parentNode.getNode(), edgeInfo);
                }
                parentNode = current;
                currentDistance = 0;
            } else {
                previousNode = current;
            }
            currentNodeNumber++;
        }
        parentNode.getNode().addNeighbour(lastNode.getNode(), new EdgeInfo(way.getId(), currentDistance));
    }

}
