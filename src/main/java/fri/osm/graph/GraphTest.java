package fri.osm.graph;

import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.routing.util.FlagEncoderFactory;
import com.graphhopper.storage.GraphBuilder;
import com.graphhopper.storage.GraphHopperStorage;
import com.graphhopper.storage.GraphStorage;
import com.graphhopper.storage.RAMDirectory;
import com.graphhopper.storage.index.LocationIndex;
import com.graphhopper.storage.index.LocationIndexTree;

import java.io.File;


public class GraphTest {

    public static void main(String[] args) {
        String osmFile = "netherlands-latest.osm.pbf";
        String dir = "graphHopper";
        GraphBuilder builder = new GraphBuilder(new EncodingManager(FlagEncoderFactory.CAR)).setLocation(dir).setStore(true);
        GraphStorage graph = builder.create();
        graph.flush();
        GraphHopperStorage load = builder.load();
        System.out.println("end");
        LocationIndex index = new LocationIndexTree(load.getBaseGraph(), new RAMDirectory(new File(dir).getAbsolutePath(), true));
    }
}
