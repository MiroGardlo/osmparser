package fri.osm;

import fri.osm.service.FileService;
import fri.osm.service.OverpassQueryService;
import fri.osm.utils.AmenityTask;

import java.util.List;

public class App {

    public static void main(String[] args) {
        OverpassQueryService queryService = OverpassQueryService.getInstance();
        FileService service = new FileService();
        List<AmenityTask> tasks = service.readTasks("tasks.txt");
        TaskRunner runner = new TaskRunner(tasks, queryService);
        runner.run();
    }
}
