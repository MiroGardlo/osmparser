package fri.osm.utils;

import fri.osm.model.ChargingStation;

public class QueryHelper {


    public static String buildAmenityQuery(ChargingStation station, double radius, String amenity) {
        String query = new OverpassQueryBuilder()
                .union()
                .node()
                .withRadiusAround(radius, station.getLatitude(), station.getLongitude())
                .withAmenity(amenity)
                .endStatement()
                .referenceWaysAndRelations()
                .endUnion()
                .build();
        System.out.println("station id: " + station.getId() + " Query: " + query.toString());
        return query;
    }

    public static String buildNetherlandsAmenityQuery(String amenityName) {
        return String.format("[out:xml][timeout:900];area[name=Nederland]->.boundaryarea;" +
                "(node(area.boundaryarea)[amenity=%s];" +
                "way(area.boundaryarea)[amenity=%s];>;" +
                "rel(area.boundaryarea)[amenity=%s];>;);" +
                "out body;", amenityName, amenityName, amenityName);
    }

    public static String buildTestQuery() {
        return new OverpassQueryBuilder()
                .union()
                .area()
                .withTag("name", "Bonn")
                .endStatement()
                .nodeWithAreaSpecified()
                .withAmenity("restaurant")
                .endStatement()
                .referenceWaysAndRelations()
                .endUnion()
                .build();
    }

    public static String buildTestRelationQuery() {
        return "[out:xml];\n" +
                "area[name=London]->.boundaryarea;(\n" +
                "   rel(area.boundaryarea)[amenity=restaurant];>;)\n" +
                "  ;\n" +
                "out body;";
    }

}
