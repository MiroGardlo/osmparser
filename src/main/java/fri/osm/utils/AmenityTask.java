package fri.osm.utils;

public class AmenityTask {
    private String amenity;

    public AmenityTask(String amenity) {
        this.amenity = amenity;
    }

    public String getAmenity() {
        return amenity;
    }

}
