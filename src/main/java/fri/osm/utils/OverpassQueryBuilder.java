package fri.osm.utils;

import java.util.Locale;

public class OverpassQueryBuilder {
    StringBuilder query;
    private boolean isUnion;

    public OverpassQueryBuilder() {
        this.query = new StringBuilder();
        this.query.append("[out:xml];");
        this.isUnion = false;
    }

    public OverpassQueryBuilder union() {
        this.isUnion = true;
        this.query.append("(");
        return this;
    }

    public OverpassQueryBuilder endUnion() {
        if (isUnion) {
            query.append(")");
        }
        this.isUnion = false;
        return this;
    }

    public OverpassQueryBuilder withRadiusAround(double radiusInMetres, double latitude, double longitude) {
        this.query.append(String.format(Locale.ENGLISH, "(around: %f, %f, %f)", radiusInMetres, latitude, longitude));
        return this;
    }

    public OverpassQueryBuilder node() {
        this.query.append("node");
        return this;
    }

    public OverpassQueryBuilder nodeWithAreaSpecified() {
        this.query.append("node(area)");
        return this;
    }

    public OverpassQueryBuilder area() {
        this.query.append("area");
        return this;
    }

    public OverpassQueryBuilder withAmenity(String amenity) {
        return withTag("amenity", amenity);
    }

    public OverpassQueryBuilder withTag(String key, String value) {
        this.query.append(String.format("[%s=%s]", key, value));
        return this;
    }

    public OverpassQueryBuilder endStatement() {
        this.query.append(";");
        return this;
    }

    public OverpassQueryBuilder referenceWaysAndRelations() {
        this.query.append(" < ;");
        return this;
    }

    public String build() {
        query.append("; out body;");
        return query.toString();
    }
}
