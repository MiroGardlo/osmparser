package fri.osm;

import fri.osm.graph.OSMFilter;
import fri.osm.graph.model.Graph;
import fri.osm.model.OSMStructure;
import fri.osm.service.XMLService;

import java.io.FileInputStream;
import java.io.FileNotFoundException;


public class RoadNetworkTest {

    public static void main(String[] args) {
        try {
            OSMStructure osmStructure = XMLService.unmarshalXml(new FileInputStream("netherlands_roadnetwork.osm_01.osm"));
            System.out.println("Network read");
            OSMFilter filter = new OSMFilter(osmStructure);
            Graph graph = filter.init();
            System.out.println(graph.getNodes().size());
            boolean graphConnected = graph.isGraphConnected();
            if (graphConnected) {
                System.out.println("Graph connected!");
            } else {
                System.out.println("Graph not connected!");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
