package fri.osm.service;

import fri.osm.model.HelperWay;

public class DirectionHelper {
    private static String oneWayString = "oneway";
    private static String highwayString = "highway";
    private static String junctionString = "junction";

    public static boolean isWayOneWay(HelperWay way) {
        if (way.hasTag(oneWayString)) {
            String value = way.getTagValue(oneWayString).toLowerCase();
            if (value.equals("yes") || value.equals("-1")) {
                return true;
            }
            return false;
        } else if (way.hasTag(highwayString)) {
            String value = way.getTagValue(highwayString).toLowerCase();
            return value.equals("motorway");
        } else if (way.hasTag(junctionString)) {
            String value = way.getTagValue(junctionString).toLowerCase();
            return value.equals("roundabout");
        }
        return false;
    }
}
