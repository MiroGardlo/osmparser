package fri.osm.service;

import fri.osm.model.OSMStructure;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.InputStream;


public class XMLService {

    public static void marshalXml(OSMStructure response, String fileName) {
        JAXBContext jaxbContext;
        try {
            jaxbContext = XMLService.createContext();
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(response, new File(fileName));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static OSMStructure unmarshalXml(InputStream stream) {
        JAXBContext jaxbContext = createContext();
        Unmarshaller jaxbUnmarshaller = getUnmarshaller(jaxbContext);
        OSMStructure response;
        try {
            response = (OSMStructure) jaxbUnmarshaller.unmarshal(stream);
        } catch (JAXBException e) {
            throw new RuntimeException(e.getMessage());
        }
        return response;
    }


    public static Unmarshaller getUnmarshaller(JAXBContext jaxbContext) {
        try {
            return jaxbContext.createUnmarshaller();
        } catch (JAXBException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static JAXBContext createContext() {
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(OSMStructure.class);
            return jaxbContext;
        } catch (JAXBException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }
}
