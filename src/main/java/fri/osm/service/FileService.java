package fri.osm.service;

import fri.osm.utils.AmenityTask;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class FileService {

    public void printToFile(String line, PrintWriter writer) {
        writer.println(line);
    }

    public List<AmenityTask> readTasks(String filePath) {
        List<AmenityTask> tasks = new ArrayList<>(50);
        try (Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(filePath)))) {
            while (sc.hasNext()) {
                tasks.add(new AmenityTask(sc.nextLine()));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return tasks;
    }
}
