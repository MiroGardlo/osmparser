package fri.osm.service;


import fri.osm.model.OSMStructure;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class OverpassQueryService {

    private static int currentServer = 0;
    private final static List<String> overpassServers = initServerUrl();
    private static OverpassQueryService overpassQueryService = new OverpassQueryService();

    private static List<String> initServerUrl() {
        List<String> servers = new ArrayList<>();
        servers.add("http://overpass-api.de/api/interpreter");
        servers.add("http://overpass.osm.rambler.ru/cgi/interpreter");
        servers.add("http://api.openstreetmap.fr/oapi/interpreter");
        return servers;
    }

    private OverpassQueryService() {

    }

    public static OverpassQueryService getInstance() {
        return overpassQueryService;
    }

    public OSMStructure getOverpassQueryResponse(String overpassQuery, String amenity) {
        HttpURLConnection connection = getHttpURLConnection();
        prepareRequest(overpassQuery, connection);
        return unmarshalXml(connection, overpassQuery, amenity);
    }

    private void prepareRequest(String overpassQuery, HttpURLConnection connection) {
        try {
            DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes("data=" + URLEncoder.encode(overpassQuery, "utf-8"));
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void createXmlFile(String overpassQuery, String fileName, String amenity) {
        OSMStructure overpassQueryResponse = getOverpassQueryResponse(overpassQuery, amenity);
        XMLService.marshalXml(overpassQueryResponse, fileName);
    }


    private void waitAndRetry(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }

    private OSMStructure unmarshalXml(HttpURLConnection connection, String query, String amenity) {
        JAXBContext jaxbContext = XMLService.createContext();
        Unmarshaller jaxbUnmarshaller = XMLService.getUnmarshaller(jaxbContext);
        OSMStructure response = null;
        HttpURLConnection currentConnection = connection;
        boolean sendRequest = true;

        while (sendRequest) {
            try {
                System.out.println("Processing: " + amenity);
                long time = System.currentTimeMillis();
                response = (OSMStructure) jaxbUnmarshaller.unmarshal(currentConnection.getInputStream());
                time = (System.currentTimeMillis() - time) / 1000;
                System.out.println(time);
                sendRequest = false;
                System.out.println("Query processed successfully!");
                waitAndRetry(4000);
            } catch (IOException e) {
                System.out.println(e.getMessage());
                System.out.println("Waiting...");
                waitAndRetry(5000);
                currentServer = (currentServer + 1) % overpassServers.size();
                System.out.println("Retrying query using server: " + overpassServers.get(currentServer));
                currentConnection = getHttpURLConnection();
                prepareRequest(query, currentConnection);
                sendRequest = true;
            } catch (Exception e) {
                sendRequest = false;
                System.out.println(e.getMessage() + "\nProgram ended unsuccessfully!!!!!");
            }
        }
        return response;
    }


    private HttpURLConnection getHttpURLConnection() {
        HttpURLConnection connection;
        try {
            URL url = new URL(overpassServers.get(currentServer));
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return connection;
    }
}
