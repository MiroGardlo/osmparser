package fri.osm;


import fri.osm.service.OverpassQueryService;
import fri.osm.utils.AmenityTask;
import fri.osm.utils.QueryHelper;

import java.util.List;

public class TaskRunner {
    private List<AmenityTask> tasks;
    private OverpassQueryService queryService;

    public TaskRunner(List<AmenityTask> tasks, OverpassQueryService queryService) {
        this.tasks = tasks;
        this.queryService = queryService;
    }

    public void run() {
        for (AmenityTask task : tasks) {
            String query = QueryHelper.buildNetherlandsAmenityQuery(task.getAmenity());
            System.out.println("Query: " + query);
            queryService.createXmlFile(query, task.getAmenity() + ".osm", task.getAmenity());
        }
    }

}
