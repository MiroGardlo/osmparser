package fri.osm.model;

import fri.osm.graph.model.GraphNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;


public class HelperWay {
    private long id;
    private List<GraphNode> referencedNodes;
    private HashMap<String, String> tags;

    public HelperWay(Way way) {
        this.id = way.getId();
        this.referencedNodes = new ArrayList<>(way.getReferencedNodes().size());
        initTags(way);
    }

    private void initTags(Way way) {
        this.tags = new HashMap(way.getTags().stream().collect(Collectors.toMap(tag -> tag.getKey(), tag -> tag.getValue())));
    }

    public boolean hasTag(String key) {
        return tags.containsKey(key);
    }

    public void addNode(GraphNode node) {
        this.referencedNodes.add(node);
    }

    public long getId() {
        return id;
    }

    public String getTagValue(String tag) {
        return this.tags.get(tag);
    }
}
