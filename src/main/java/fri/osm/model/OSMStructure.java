package fri.osm.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "osm")
@XmlAccessorType(XmlAccessType.FIELD)
public class OSMStructure {

    @XmlElement(name = "node")
    private List<Node> nodes;
    @XmlElement(name = "way")
    private List<Way> ways;
    @XmlElement(name = "relation")
    private List<Relation> relations;


    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public List<Way> getWays() {
        return ways;
    }

    public void setWays(List<Way> ways) {
        this.ways = ways;
    }

    public List<Relation> getRelations() {
        return relations;
    }

    public void setRelations(List<Relation> relations) {
        this.relations = relations;
    }

    public int numberOfNodes() {
        if (this.nodes != null) {
            return this.nodes.size();
        }
        return 0;
    }

    public int numberOfWays() {
        if (this.ways != null) {
            return this.ways.size();
        }
        return 0;
    }

}
