package fri.osm.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)

public class Way {

    @XmlAttribute(name = "id")
    private Long id;
    @XmlElement(name = "nd")
    private ArrayList<NodalReference> referencedNodes;
    @XmlElement(name = "tag")
    private List<Tag> tags;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ArrayList<NodalReference> getReferencedNodes() {
        return referencedNodes;
    }

    public void setReferencedNodes(ArrayList<NodalReference> referencedNodes) {
        this.referencedNodes = referencedNodes;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

}
