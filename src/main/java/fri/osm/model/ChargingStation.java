package fri.osm.model;

public class ChargingStation {
    private long id;
    private double latitude;
    private double longitude;

    public ChargingStation(String lineInput) {
        String[] data = lineInput.split(",");
        this.id = Long.parseLong(data[0]);
        this.latitude = Double.valueOf(data[1]);
        this.longitude = Double.valueOf(data[2]);
    }

    public long getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
